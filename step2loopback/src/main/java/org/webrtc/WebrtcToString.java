package org.webrtc;

public class WebrtcToString {
    public static String toString(VideoFrame videoFrame) {
        if (videoFrame == null) {
            return "null";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("VideoFrame, rotation: ")
                .append(videoFrame.getRotation())
                .append("\n rotated width: ")
                .append(videoFrame.getRotatedWidth())
                .append("\n rotated height: ")
                .append(videoFrame.getRotatedHeight())
                .append("\n time stamp in ns: ")
                .append(videoFrame.getTimestampNs());
        VideoFrame.Buffer buffer = videoFrame.getBuffer();
        if (buffer instanceof VideoFrame.TextureBuffer) {
            VideoFrame.TextureBuffer textureBuffer = (VideoFrame.TextureBuffer) buffer;
            stringBuilder.append("\n TextureBuffer, texture id: ")
                    .append(textureBuffer.getTextureId())
                    .append("\n type: ")
                    .append(textureBuffer.getType())
                    .append("\n buffer type: ")
                    .append(textureBuffer.getBufferType())
                    .append("\n width: ")
                    .append(textureBuffer.getWidth())
                    .append("\n height: ")
                    .append(textureBuffer.getHeight())
                    .append("\n transform matrix: ")
                    .append(textureBuffer.getTransformMatrix());
        } else if (buffer instanceof VideoFrame.I420Buffer) {
            VideoFrame.I420Buffer i420Buffer = (VideoFrame.I420Buffer) buffer;
            stringBuilder.append("\n TextureBuffer, buffer type: ")
                    .append(i420Buffer.getBufferType())
                    .append("\n width: ")
                    .append(i420Buffer.getWidth())
                    .append("\n height: ")
                    .append(i420Buffer.getHeight())
                    .append("\n Y data: ")
                    .append(i420Buffer.getDataY())
                    .append("\n Y stride: ")
                    .append(i420Buffer.getStrideY())
                    .append("\n U data: ")
                    .append(i420Buffer.getDataU())
                    .append("\n U stride: ")
                    .append(i420Buffer.getStrideU())
                    .append("\n V data: ")
                    .append(i420Buffer.getDataV())
                    .append("\n V stride: ")
                    .append(i420Buffer.getStrideV());
        }
        return stringBuilder.toString();
    }

    public static String toString(EncodedImage encodedImage) {
        if (encodedImage == null) {
            return "null";
        }
        return "EncodedImage, width: " + encodedImage.encodedWidth +
                "\n height: " +
                encodedImage.encodedHeight +
                "\n time in ms: " +
                encodedImage.captureTimeMs +
                "\n time in ns: " +
                encodedImage.captureTimeNs +
                "\n rotation: " +
                encodedImage.rotation +
                "\n buffer: " +
                encodedImage.buffer +
                "\n frame type: " +
                encodedImage.frameType +
                "\n qp: " +
                encodedImage.qp;
    }

    public static String toString(VideoDecoder.DecodeInfo decodeInfo) {
        if (decodeInfo == null) {
            return "null";
        }
        return "DecodeInfo, renderTimeMs: " + decodeInfo.renderTimeMs +
                "\n is missing frames: " +
                decodeInfo.isMissingFrames;
    }

    public static String toString(VideoDecoder.Settings settings) {
        if (settings == null) {
            return "null";
        }

        return "VideoDecoder Settings, width: " + settings.width +
                "\n height: " +
                settings.height +
                "\n numberOfCores: " +
                settings.numberOfCores;
    }

}

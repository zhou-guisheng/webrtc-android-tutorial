package org.webrtc;

import android.util.Log;

import java.util.Map;

public class MyVideoEncoder implements VideoEncoder, VideoEncoder.Callback  {
    private static final String TAG = MyVideoEncoder.class.getSimpleName();

    private VideoEncoder videoEncoder;
    private VideoEncoder.Callback nativeCallback;

    public MyVideoEncoder(DefaultVideoEncoderFactory defaultVideoEncoderFactory, VideoCodecInfo videoCodecInfo) {
        videoEncoder = defaultVideoEncoderFactory.createEncoder(videoCodecInfo);
    }

    @Override
    public long createNativeVideoEncoder() {
        Log.i(TAG, "createNativeVideoEncoder()");
        return videoEncoder.createNativeVideoEncoder();
    }

    @Override
    public boolean isHardwareEncoder() {
        Log.i(TAG, "isHardwareEncoder()");
        return videoEncoder.isHardwareEncoder();
    }

    @Override
    public VideoCodecStatus initEncode(Settings settings, Callback callback) {
        Log.i(TAG, "initEncode()");
        this.nativeCallback = callback;
        return videoEncoder.initEncode(settings, this);
    }

    @Override
    public VideoCodecStatus release() {
        Log.i(TAG, "release()");
        return videoEncoder.release();
    }

    @Override
    public VideoCodecStatus encode(VideoFrame videoFrame, EncodeInfo encodeInfo) {
        Log.i(TAG, "encode()");
        return videoEncoder.encode(videoFrame, encodeInfo);
    }

    @Override
    public VideoCodecStatus setRateAllocation(BitrateAllocation bitrateAllocation, int i) {
        Log.i(TAG, "setRateAllocation()");
        return videoEncoder.setRateAllocation(bitrateAllocation, i);
    }

    @Override
    public ScalingSettings getScalingSettings() {
        Log.i(TAG, "getScalingSettings()");
        return videoEncoder.getScalingSettings();
    }

    @Override
    public ResolutionBitrateLimits[] getResolutionBitrateLimits() {
        Log.i(TAG, "getResolutionBitrateLimits()");
        return videoEncoder.getResolutionBitrateLimits();
    }

    @Override
    public String getImplementationName() {
        Log.i(TAG, "getImplementationName()");
        return videoEncoder.getImplementationName();
    }

    @Override
    public void onEncodedFrame(EncodedImage encodedImage, CodecSpecificInfo codecSpecificInfo) {
        Log.i(TAG, "onEncodedFrame(), encodedImage: " + WebrtcToString.toString(encodedImage) + ", codecSpecificInfo: " + codecSpecificInfo, new Exception(""));
        if (nativeCallback != null) {
            nativeCallback.onEncodedFrame(encodedImage, codecSpecificInfo);
        }
    }
}

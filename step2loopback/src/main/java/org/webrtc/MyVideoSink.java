package org.webrtc;

import android.util.Log;

/**
 * 接收解码后的视频数据
 */
public class MyVideoSink implements VideoSink {
    private static final String TAG = MyVideoSink.class.getSimpleName();

    @Override
    public void onFrame(VideoFrame videoFrame) {
        Log.i(TAG, "onFrame(), " + WebrtcToString.toString(videoFrame), new Exception(""));
    }


}

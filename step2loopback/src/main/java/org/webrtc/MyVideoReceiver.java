package org.webrtc;

import android.util.Log;

import androidx.annotation.Nullable;

/**
 * 接收从远端获取的编码数据
 */
public class MyVideoReceiver implements VideoDecoder {
    private static final String TAG = MyVideoReceiver.class.getSimpleName();

    private String codecName;

    public MyVideoReceiver(String codecName, VideoCodecMimeType codecType, int colorFormat, @Nullable EglBase.Context sharedContext) {
        this.codecName = codecName;
        Log.i(TAG, "MyVideoReceiver(), codecName: " + codecName + ", codecType: " + codecType + ", colorFormat: " + colorFormat + ", sharedContext: " + sharedContext);
    }

    @Override
    public long createNativeVideoDecoder() {
        Log.i(TAG, "createNativeVideoDecoder()");
        return 0L;
    }

    @Override
    public VideoCodecStatus initDecode(Settings settings, Callback callback) {
        Log.i(TAG, "initDecode(), " + WebrtcToString.toString(settings));
        return VideoCodecStatus.OK;
    }

    @Override
    public VideoCodecStatus release() {
        Log.i(TAG, "release()");
        return VideoCodecStatus.OK;
    }

    @Override
    public VideoCodecStatus decode(EncodedImage encodedImage, DecodeInfo decodeInfo) {
        Log.i(TAG, "encodedImage: " + WebrtcToString.toString(encodedImage) + ", decodeInfo: " + WebrtcToString.toString(decodeInfo)
                + "\n data: " + ByteUtils.toHexString(encodedImage.buffer, 16), new Exception("test"));
        return VideoCodecStatus.OK;
    }

    @Override
    public String getImplementationName() {
        Log.i(TAG, "getImplementationName(), name: " + codecName);
        return codecName;
    }

}

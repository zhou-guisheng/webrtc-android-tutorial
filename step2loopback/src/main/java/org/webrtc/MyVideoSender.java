package org.webrtc;

import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.Nullable;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

/**
 * 接收从远端获取的编码数据
 */
public class MyVideoSender implements VideoEncoder {
    private static final String TAG = MyVideoSender.class.getSimpleName();

    private String codecName;
    private Callback nativeCallback;

    public MyVideoSender(String codecName, VideoCodecMimeType codecType, int colorFormat, @Nullable EglBase.Context sharedContext) {
        this.codecName = codecName;
        Log.i(TAG, "MyVideoReceiver(), codecName: " + codecName + ", codecType: " + codecType + ", colorFormat: " + colorFormat + ", sharedContext: " + sharedContext);
    }

    @Override
    public long createNativeVideoEncoder() {
        Log.i(TAG, "createNativeVideoEncoder()");
        return 0L;
    }

    @Override
    public boolean isHardwareEncoder() {
        Log.i(TAG, "isHardwareEncoder()");
        return false;
    }

    @Override
    public VideoCodecStatus initEncode(Settings settings, Callback callback) {
        this.nativeCallback = callback;
        return VideoCodecStatus.OK;
    }

    @Override
    public VideoCodecStatus release() {
        Log.i(TAG, "release()");
        return VideoCodecStatus.OK;
    }

    @Override
    public VideoCodecStatus encode(VideoFrame videoFrame, EncodeInfo encodeInfo) {
        Log.i(TAG, "encode()");
        return VideoCodecStatus.OK;
    }

    @Override
    public VideoCodecStatus setRateAllocation(BitrateAllocation bitrateAllocation, int i) {
        Log.i(TAG, "setRateAllocation()");
        return VideoCodecStatus.OK;
    }

    @Override
    public ScalingSettings getScalingSettings() {
        Log.i(TAG, "getScalingSettings()");
        return new ScalingSettings(20, 30);
    }

    @Override
    public String getImplementationName() {
        return codecName;
    }

    /**
     * 提供给外部调用，告诉有新的编码数据可以进行解码发送
     */
    public void onFrame(EncodedImage encodedImage) {
        Log.i(TAG, "onFrame(), " + encodedImage);
        if (nativeCallback == null) {
            return;
        }
        this.nativeCallback.onEncodedFrame(encodedImage, new CodecSpecificInfo());
    }

    /**
     * 提供给外部调用，告诉有新的编码数据可以进行解码发送
     */
    public void onFrame(ByteBuffer encodedBuffer, int width, int height, long timeInNs, int rotation, boolean isKeyFrame) {
        Log.i(TAG, "onFrame(), " + encodedBuffer);
        if (nativeCallback == null) {
            return;
        }

        EncodedImage.FrameType frameType = isKeyFrame ? EncodedImage.FrameType.VideoFrameKey : EncodedImage.FrameType.VideoFrameDelta;
        EncodedImage encodedImage =  EncodedImage.builder()
                .setEncodedHeight(width)
                .setEncodedHeight(height)
                .setRotation(rotation)
                .setCaptureTimeMs(timeInNs/1000L)
                .setCaptureTimeNs(timeInNs)
                .setBuffer(encodedBuffer, () -> {
                    Log.e(TAG, "onFrame releaseCallback");
                }).setFrameType(frameType)
                .createEncodedImage();
        this.nativeCallback.onEncodedFrame(encodedImage, new CodecSpecificInfo());
        encodedImage.release();
    }
}

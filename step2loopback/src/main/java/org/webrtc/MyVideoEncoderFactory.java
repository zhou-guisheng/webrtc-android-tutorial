package org.webrtc;

import android.util.Log;

public class MyVideoEncoderFactory implements VideoEncoderFactory {
    private static final String TAG = MyVideoEncoderFactory.class.getSimpleName();

    private EglBase.Context sharedContext;
    private DefaultVideoEncoderFactory defaultVideoEncoderFactory;

    public MyVideoEncoderFactory(EglBase.Context eglContext, boolean enableIntelVp8Encoder, boolean enableH264HighProfile) {
        Log.i(TAG, "MyVideoEncoderFactory(), eglContext: " + eglContext);
        sharedContext = eglContext;
        defaultVideoEncoderFactory = new DefaultVideoEncoderFactory(eglContext, enableIntelVp8Encoder, enableH264HighProfile);
    }

    @Override
    public VideoEncoder createEncoder(VideoCodecInfo videoCodecInfo) {
        Log.i(TAG, "createEncoder(), codecType: " + videoCodecInfo);
        return new MyVideoEncoder(defaultVideoEncoderFactory, videoCodecInfo);
    }

    @Override
    public VideoCodecInfo[] getSupportedCodecs() {
        Log.i(TAG, "getSupportedCodecs()");
        return defaultVideoEncoderFactory.getSupportedCodecs();
    }

    @Override
    public VideoCodecInfo[] getImplementations() {
        Log.i(TAG, "getImplementations()");
        return defaultVideoEncoderFactory.getImplementations();
    }

    @Override
    public VideoEncoderSelector getEncoderSelector() {
        Log.i(TAG, "getEncoderSelector()");
        return defaultVideoEncoderFactory.getEncoderSelector();
    }
}

package org.webrtc;

import java.nio.ByteBuffer;

public class ByteUtils {

    public static String toHexString(ByteBuffer buffer, int maxLen) {
        StringBuilder stringBuilder = new StringBuilder(" ");
        int pos = buffer.position();
        int limit = buffer.limit();
        if (pos + maxLen < limit) {
            limit = pos + maxLen;
        }
        for (int i = pos; i < limit; i++) {
            stringBuilder.append(String.format("%02x ", buffer.get(i + pos)));
        }
        return stringBuilder.toString();
    }
}

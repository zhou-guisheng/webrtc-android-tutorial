package org.webrtc;

import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 解码器工厂，用于提供自定义的解码器
 */
public class MyVideoDecoderFactory implements VideoDecoderFactory {
    private static final String TAG = MyVideoDecoderFactory.class.getSimpleName();

    private EglBase.Context sharedContext;
    public MyVideoDecoderFactory(@Nullable EglBase.Context eglContext) {
        Log.i(TAG, "MyVideoDecoderFactory(), eglContext: " + eglContext);
        this.sharedContext = eglContext;
    }

    public VideoDecoder createDecoder(String codecType) {
        Log.i(TAG, "createDecoder(), codecType: " + codecType);
        return null;
    }

    @Override
    public VideoDecoder createDecoder(VideoCodecInfo codecType) {
        Log.i(TAG, "createDecoder(), info: " + codecType);
        VideoCodecMimeType type = VideoCodecMimeType.valueOf(codecType.getName());
        MediaCodecInfo info = this.findCodecForType(type.mimeType());
        if (info == null) {
            return null;
        } else {
            MediaCodecInfo.CodecCapabilities capabilities = info.getCapabilitiesForType(type.mimeType());
            return new MyVideoReceiver(info.getName(), type, MediaCodecUtils.selectColorFormat(MediaCodecUtils.DECODER_COLOR_FORMATS, capabilities), this.sharedContext);
        }
    }

    @Override
    public VideoCodecInfo[] getSupportedCodecs() {
        List<VideoCodecInfo> supportedCodecInfos = new ArrayList();
        String[] types = new String[]{"video/avc"/*, "video/hevc"*/};

        for(int var4 = 0; var4 < types.length; ++var4) {
            String type = types[var4];
            MediaCodecInfo codec = this.findCodecForType(type);
            if (codec != null) {
                supportedCodecInfos.add(new VideoCodecInfo(VideoCodecMimeType.H264.name(), H264Utils.getDefaultH264Params(false)));
            }
        }
        VideoCodecInfo[] infoArr = (VideoCodecInfo[])supportedCodecInfos.toArray(new VideoCodecInfo[supportedCodecInfos.size()]);
        Log.i(TAG, "getSupportedCodecs(), codecType: " + Arrays.deepToString(infoArr));
        return infoArr;
    }

    private MediaCodecInfo findCodecForType(String type) {
        MediaCodecList mediaCodecList = new MediaCodecList(MediaCodecList.REGULAR_CODECS);
        for (MediaCodecInfo info : mediaCodecList.getCodecInfos()) {
            if (info != null && !info.isEncoder() && this.isSupportedCodec(info, type)) {
                return info;
            }
        }
        return null;

    }
    private boolean isSupportedCodec(MediaCodecInfo info, String type) {
        VideoCodecMimeType codecMimeType = VideoCodecMimeType.H264;
        if (!MediaCodecUtils.codecSupportsType(info, codecMimeType)) {
            return false;
        } else {
            return MediaCodecUtils.selectColorFormat(MediaCodecUtils.DECODER_COLOR_FORMATS, info.getCapabilitiesForType(type)) != null && this.isCodecAllowed(info);
        }
    }

    private boolean isCodecAllowed(MediaCodecInfo info) {
        return MediaCodecUtils.isHardwareAccelerated(info);
    }
}
